<?php

class views_handler_metatag_data extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['data'] = 'data';
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['metatag_field'] = array('default' => 'title');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $info = metatag_get_info('tags');

    foreach (array_keys($info) as $key => $value) {
      $fields[$value] = $value;
    }

    $form['metatag_field'] = array(
      '#title' => t('Which Tag'),
      '#type' => 'select',
      '#options' => $fields,
      '#default_value' => $this->options['metatag_field'],
    );
  }

  function render($values) {
    $data = unserialize($values->{$this->aliases['data']});
    $selected = $this->options['metatag_field'];
    if (!empty($data[$selected]['value'])) {
      return $data[$selected]['value'];
    }
  }
}
